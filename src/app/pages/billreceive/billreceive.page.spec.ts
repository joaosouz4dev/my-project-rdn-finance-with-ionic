import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { BillReceivePage } from "./billreceive.page";

describe("BillReceivePage", () => {
  let component: BillReceivePage;
  let fixture: ComponentFixture<BillReceivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BillReceivePage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BillReceivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
