import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { StorageService } from "src/app/services/storage.service";
import { environment } from "src/environments/environment";
import { ToastService } from "src/app/services/toast.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  data = {
    email: "",
    password: ""
  };

  constructor(
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService,
    private toastService: ToastService,
    private loading: LoadingController
  ) {}

  ngOnInit() {}

  login() {
    if (this.formValidate()) {
      this.presentLoading();
      this.authService.login(this.data).subscribe(
        (response: any) => {
          if (response) {
            this.storageService.store(environment.AUTH_TOKEN, response);
            this.storageService.setLocalStorage(
              environment.AUTH_TOKEN,
              response
            );
            this.closeLoading();
            this.router.navigate(["home/dashboard"]);
          } else {
            this.closeLoading();
            this.toastService.showToast(
              "E-mail ou senha inválidos, favor verificar"
            );
          }
        },
        (err: any) => {
          this.closeLoading();
          this.toastService.showErrorToast(err);
        }
      );
    } else {
      this.closeLoading();
      this.toastService.showToast("Informe seu e-mail e senha");
    }
  }

  formValidate() {
    let email = this.data.email.trim();
    let password = this.data.password.trim();

    return (
      this.data.email && this.data.password && email.length && password.length
    );
  }

  async presentLoading() {
    const loading = await this.loading.create({
      message: "Aguarde..."
    });
    await loading.present();
  }

  async closeLoading() {
    await this.loading.dismiss();
  }
}
