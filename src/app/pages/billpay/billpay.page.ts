import { Component, OnInit } from "@angular/core";
import { ToastService } from "src/app/services/toast.service";
import { AlertController, LoadingController } from "@ionic/angular";
import { BillpayService } from "src/app/services/billpay.service";
import { CategoryService } from "src/app/services/category.service";
import * as moment from "moment";

@Component({
  selector: "app-billpay",
  templateUrl: "./billpay.page.html",
  styleUrls: ["./billpay.page.scss"]
})
export class BillPayPage implements OnInit {
  form: any = {
    category_id: null,
    date_launch: null,
    name: null,
    value: null,
    status: 1
  };

  bill_pays: any;
  categories: any;

  idAlt: Number = null;

  editable: boolean = false;

  constructor(
    public service: BillpayService,
    public category: CategoryService,
    public toast: ToastService,
    private alertCtrl: AlertController,
    private loading: LoadingController
  ) {}

  ngOnInit() {
    this.getCategories();
    this.index();
  }

  getCategories() {
    this.category.index().subscribe(
      (res: any) => {
        this.categories = res.data;
      },
      (err: any) => {
        this.categories = [];
        this.toast.showErrorToast(err);
      }
    );
  }

  index() {
    this.presentLoading();
    this.service.index().subscribe(
      (res: any) => {
        this.bill_pays = res.data;
        this.closeLoading();
      },
      (err: any) => {
        this.bill_pays = [];
        this.closeLoading();
        this.toast.showErrorToast(err);
      }
    );
  }

  save() {
    if (
      this.form.category_id !== null &&
      this.form.date_launch !== null &&
      this.form.name !== null &&
      this.form.value !== null
    ) {
      this.presentLoading();
      this.form.date_launch = moment(this.form.date_launch).format('YYYY-MM-DD')
      this.service.store(this.form).subscribe(
        res => {
          this.closeLoading();
          this.toast.showToast("Conta adicionada com sucesso");
          this.reset();
          this.editable = false;
          this.index();
        },
        (err: any) => {
          this.closeLoading();
          this.toast.showErrorToast(err);
        }
      );
    }
  }

  edit(id: Number) {
    this.presentLoading();
    this.service.show(id).subscribe(
      (res: any) => {
        this.closeLoading();
        this.idAlt = res.data.id;
        this.form.category_id = parseInt(res.data.category_id, 10);
        this.form.date_launch = res.data.date_launch;
        this.form.name = res.data.name;
        this.form.value = res.data.value;
        this.form.status = res.data.status ? 1 : 0;
        this.editable = true;
      },
      (err: any) => {
        this.closeLoading();
        this.editable = false;
        this.toast.showErrorToast(err);
      }
    );
  }

  update() {
    if (
      this.form.category_id !== null &&
      this.form.date_launch !== null &&
      this.form.name !== null &&
      this.form.value !== null &&
      this.idAlt !== null
    ) {
      this.presentLoading();
      this.service.update(this.form, this.idAlt).subscribe(
        res => {
          this.closeLoading();
          this.toast.showToast("Conta atualizada com sucesso");
          this.reset();
          this.editable = false;
          this.index();
        },
        (err: any) => {
          this.closeLoading();
          this.toast.showErrorToast(err);
        }
      );
    }
  }

  del(id: Number) {
    this.presentLoading();
    this.service.destroy(id).subscribe(
      res => {
        this.closeLoading();
        this.toast.showToast("Conta deletada com sucesso");
        this.reset();
        this.editable = false;
        this.index();
      },
      (err: any) => {
        this.closeLoading();
        this.toast.showErrorToast(err);
      }
    );
  }

  reset() {
    this.form = {
      category_id: null,
      date_launch: null,
      name: null,
      value: null,
      status: 1
    };
    this.idAlt = null;
  }

  async presentConfirm(id: any) {
    let alert = await this.alertCtrl.create({
      message: "Confirmar a exclusão do item?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Confirmar",
          handler: () => {
            this.del(id);
          }
        }
      ]
    });
    alert.present();
  }

  async presentLoading() {
    const loading = await this.loading.create({
      message: "Aguarde..."
    });
    await loading.present();
  }

  async closeLoading() {
    await this.loading.dismiss();
  }
}
