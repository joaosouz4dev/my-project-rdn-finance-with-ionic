import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { BillPayPageRoutingModule } from "./billpay-routing.module";

import { BillPayPage } from "./billpay.page";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, BillPayPageRoutingModule],
  declarations: [BillPayPage]
})
export class BillPayPageModule {}
