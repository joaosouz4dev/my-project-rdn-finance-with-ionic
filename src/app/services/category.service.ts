import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { StorageService } from "./storage.service";

@Injectable({
  providedIn: "root"
})
export class CategoryService {
  private url = environment.API_URL;
  public token: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.getToken();
  }

  index() {
    console.log("categories.index");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.get(this.url + "/api/v1/categories", options);
  }

  store(data: any) {
    console.log("categories.store");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.post(this.url + "/api/v1/categories/store", data, options);
  }

  show(id: Number) {
    console.log("categories.show");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.get(this.url + "/api/v1/categories/show/" + id, options);
  }

  update(data: any, id: Number) {
    console.log("categories.update");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.put(
      this.url + "/api/v1/categories/update/" + id,
      data,
      options
    );
  }

  destroy(id: Number) {
    console.log("categories.destroy");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.delete(
      this.url + "/api/v1/categories/destroy/" + id,
      options
    );
  }

  getToken() {
    const token =
      this.storageService.getLocalStorage(environment.AUTH_TOKEN) || null;
    if (token) {
      this.token = token.access_token;
    }
  }
}
